--[[
Random map generator for Castle Story by Sauropod Studio
Copyright (c) 2013, Sim

This script is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

You may use, modify this script or translate to other languages, but please reference to me.

Email any questions to: onedayhost<at>yandex<dot>ru

based on SimplexNoise1234
Copyright (c) 2003-2011, Stefan Gustavson
--]]

local path = [[D:\Steam\SteamApps\common\Castle Story\Info\Niveaux\Random\]]

-- generic parameters
local zeroLevel = 110 -- average height
local minX,maxX = 256, 556
local minY,maxY = 256, 556
local border = 20

-- octaves
local noiseSeed = 1

local scale1 = 0.01
local scale2 = 0.02
local scale3 = 0.05
local ground = 0.05   -- there will be no land then noise is below that value
local amplitude = 30 -- total amplitude of noise in blocks
local power1 = 0.7
local power2 = 0.2
local power3 = 0.1
-- all powers should get 1 total

local treeScaleX = 0.03
local treeScaleY = 0.02
local treeLevel = 0.3 -- when treeNoise is greater than this - a tree should be planted

local greenScale = 0.04
local greenMargin = 0.4
local greenLevel = 0.125 -- above this level land will be more green than dirty

-- start info
local startZ = zeroLevel + 2
local startX,startY = math.floor((minX + maxX)/2), math.floor((minY + maxY)/2)
local startR = 10

-----------------------
--- utility
local bit = require 'bit'
local char = string.char
local band = bit.band
local bor = bit.bor
local bshr = bit.rshift
local bshl = bit.lshift
local floor = math.floor
local sqrt = math.sqrt
require'ex'

------
-- simplex perilin noise
local perm={}

local FASTFLOOR=math.floor
local function grad2d(hash, x, y )
    local h = band(hash, 7); --// Convert low 3 bits of hash code
	local u,v
	if h < 4 then u=x else u = y end --// into 8 simple gradient directions,
	if h < 4 then v=y else v = x end --// and compute the dot product with (x,y).
	if band(h,1)~=0 then u = -u end
	if band(h,2)~=0 then v = -v end
	--print (h)
    --return u + 2*v
	return u + v
end

function noise2d(x,y)
--// 2D simplex noise

	local F2=0.366025403 --// F2 = 0.5*(sqrt(3.0)-1.0)
	local G2=0.211324865 --// G2 = (3.0-Math.sqrt(3.0))/6.0

    local n0, n1, n2; --// Noise contributions from the three corners

    --// Skew the input space to determine which simplex cell we're in
    local s = (x+y)*F2; --// Hairy factor for 2D
    local xs = x + s;
    local ys = y + s;
    local i = FASTFLOOR(xs);
    local j = FASTFLOOR(ys);

    local t = (i+j)*G2;
    local X0 = i-t; --// Unskew the cell origin back to (x,y) space
    local Y0 = j-t;
    local x0 = x-X0; --// The x,y distances from the cell origin
    local y0 = y-Y0;
--	print(x0,y0)
    --// For the 2D case, the simplex shape is an equilateral triangle.
    --// Determine which simplex we are in.
    local i1, j1; --// Offsets for second (middle) corner of simplex in (i,j) coords
    if(x0>y0) then
		i1=1; j1=0; --// lower triangle, XY order: (0,0)->(1,0)->(1,1)
    else
		i1=0; j1=1; --// upper triangle, YX order: (0,0)->(0,1)->(1,1)
	end

    --// A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
    --// a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
    --// c = (3-sqrt(3))/6

    local x1 = x0 - i1 + G2; --// Offsets for middle corner in (x,y) unskewed coords
    local y1 = y0 - j1 + G2;
    local x2 = x0 - 1.0 + 2.0 * G2; --// Offsets for last corner in (x,y) unskewed coords
    local y2 = y0 - 1.0 + 2.0 * G2;

    --// Wrap the integer indices at 256, to avoid indexing perm[1+] out of bounds
    local ii = band(i, 0xff);
    local jj = band(j, 0xff);

    --// Calculate the contribution from the three corners
    local t0 = 0.5 - x0*x0-y0*y0;
    if(t0 < 0.0) then
		n0 = 0.0;
    else
      t0 = t0 * t0;
      n0 = t0 * t0 * grad2d(perm[1+ii+perm[1+jj]], x0, y0);
    end

    local t1 = 0.5 - x1*x1-y1*y1;
    if(t1 < 0.0) then
		n1 = 0.0;
    else
      t1 = t1 * t1;
      n1 = t1 * t1 * grad2d(perm[1+ii+i1+perm[1+jj+j1]], x1, y1);
    end

    local t2 = 0.5 - x2*x2-y2*y2;
    if(t2 < 0.0) then
		n2 = 0.0;
    else
      t2 = t2 * t2;
      n2 = t2 * t2 * grad2d(perm[1+ii+1+perm[1+jj+1]], x2, y2);
    end

    --// Add contributions from each corner to get the final noise value.
    --// The result is scaled to return values in the interval [-1,1].
    return 40.0 * (n0 + n1 + n2); --// TODO: The scale factor is preliminary!
end
------
local function initNoise()
	math.randomseed(noiseSeed)
	local random = math.random
	random(1,256);random(1,256) -- fix known lua bug with random
	for i = 1,256 do
		perm[i] = i-1
	end
	for i = 1,255 do
		local r = random(i,256)
		perm[i],perm[r] = perm[r],perm[i]
	end
	for i = 1,256 do
		perm[i+256] = perm[i]
	end
end

local function coord2indexObj(x,y,z)
	local blockX,blockY = bshr(x,8), bshr(y,8)
	local block = bshl(blockY,3) + blockX
	x = band(x,1023)
	y = band(y,1023)
	z = band(z,1023)
	return bor(0, bshl(z,20) + bshl(y,10) + x)
end

local randH
local addTree
do

	local borderInv = 1/border

	randH = function (x,y)
		local val, sl, tp = 0, 1, 4
		if x < minX or y < minY or x > maxX or y > maxY then
			return -1
		end
		local z =
			power1 * noise2d(x * scale1,y * scale1)
		if z > greenLevel then tp = 2 end
		if noise2d(x * greenScale - 100,y * greenScale - 200) > greenMargin then
			--inverse color sometimes
			if tp == 2 then
				tp = 4
			else
				tp = 2
			end
		end
		z = z + power2 * noise2d(x * scale2 - 100,y * scale2)
		z = z + power3 * noise2d(x * scale3 + 100,y * scale3 + 100)

		if x < minX + border then
			local t = (minX + border - x) *borderInv
			z = z * ( 1 - t * t)
		end
		if y < minY + border then
			local t = (minY +border - y) *borderInv
			z = z * ( 1 - t * t)
		end
		if x > maxX -border then
			local t = (x - maxX + border) *borderInv
			z = z * ( 1 - t * t)
		end
		if y > maxY - border then
			local t = (y - maxY + border) *borderInv
			z = z * (1 - t * t)
		end

		if z < ground then return -1 end
		z = (z - ground) * amplitude
		val = floor(z)
		sl = floor((z - val) * 3) + 1

		if z > 2
			and sl < 3
			then
			if band(x,3) ==1 and band(y,3) ==1 then
				if noise2d(x * treeScaleX, y * treeScaleY + 100) > treeLevel then
					addTree(x,y,zeroLevel + val-1)
				end
				tp = 4
			end
		end

		return val, sl, tp
	end
end
-- slope 0 means digging required
-- slope 1 means small +z
-- slope 2 means avg   +z
-- slope 3 means big   +Z
function randMap(path)
	local startR2 = startR * startR
	greenLevel = greenLevel * power1

	initNoise()

	os.mkdir(path)
	local F2 = io.open(path..'Monde_Arbre','w')
	local F3 = io.open(path..'Monde_Doodads','w')
	addTree = function (x,y,z)
		--local idx = coord2indexTree(x,y)
		local idx = coord2indexObj(x,y,z)
		F2:write(char(band(idx,255),band(bshr(idx,8),255),band(bshr(idx,16),255),band(bshr(idx,24),255) ))
	end
	local function addItem(tp,x,y,z)
		local idx = coord2indexObj(x,y,z)
		F3:write(tp..' '..idx..' ')
	end

	addItem('StartingPoint',startX,startY,startZ)
	addItem('StartingPoint',startX-1,startY,startZ)
	addItem('Crystal',startX,startY,startZ-1)
	addTree(startX - 3,startY + 3,startZ-1)

	for i = 0,31 do
		os.remove(path..'Monde_'..i)
	end
	local t = 0
	for bY = bshr(minY,8),bshr(maxY+255,8) do
		for bX = bshr(minX,8),bshr(maxX+255,8) do
			local block = 8 * bY + bX
			print(t)
			t = t +1
			local out = char(0,0)
			local needWrite = false
			for j = 0,255 do
				for i = 0,255 do
					local x = 256 * bX + i
					local y = 256 * bY + j
					local min,max,tp,slope = 0,0,0,0

					max,slope,tp = randH(x,y)
					if max <= 0 then
						min,max,slope,tp = 0, 0, 0, 0
					else
						needWrite = true
						min = zeroLevel - floor(1.2 * max) - 1
						max = zeroLevel + max
					end
					local dx,dy = x - startX, y - startY
					local r = dx * dx + dy * dy
					if r < startR2 then
						needWrite = true
						local v = startZ
						if max < v then
							max = v
							slope = 1
						end
						if min == 0 or min > max then
							min = zeroLevel - 4
						end
						tp = 2
					end
					out = out .. char(min,max,tp,slope)
				end
			end
			if needWrite then
				local F = io.open(path..'Monde_'..block,'wb')
				F:write(out)
				F:close()
			end
		end
	end
	F2:close()
	F3:close()
end
--loadChunk(path,18)
--saveChunk(path,18,loadPgm(path..'src_'..18..'.pgm') )
--saveChunk2(path,18 )
randMap(path)
