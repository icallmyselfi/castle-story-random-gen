--[[
Grayscale map generator for Castle Story by Sauropod Studio
Copyright (c) 2013, Sim

This script is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

You may use, modify this script or translate to other languages, but please reference to me.

Email any questions to: onedayhost<at>yandex<dot>ru
--]]
--[[
	Usage:
	1) create an empty folder at Castle story maps
	2) set 'path' variable to it
	3) create base.png file (in that folder) with green color component equal to height of a bottom layer
	4) create diff.png file
		a) red component set the height of a map
		b) green value over 64 means grass and over 128 means tree should be planted at that location
--]]

local path = [[D:\Steam\SteamApps\common\Castle Story\Info\Niveaux\Gray\]]

-- start info
local startX,startY -- if not set here, will be at center of the map

-----------------------
--- utility
local gd = require'gd'
local bit = require 'bit'
local re
local band = bit.band
local bor = bit.bor
local bshr = bit.rshift
local bshl = bit.lshift
local floor = math.floor
local sqrt = math.sqrt

local char = string.char

local function index2coord(idx)
	local x,y
	local block = bshr(idx,24)
	local pos = band(x,16777215)
	local blockY = bshr(block,3)
	local blockX = band(block,7)
	local zone = 0
	x = mod(pos - zone - 2 ,256) + 256 * blockX
	y = bshr(pos - zone - 2,8) + 256 * blockY
	return x,y
end

local function coord2indexObj(x,y,z)
	local blockX,blockY = bshr(x,8), bshr(y,8)
	local block = bshl(blockY,3) + blockX
	x = band(x,1023)
	y = band(y,1023)
	z = band(z,1023)
	return bor(0, bshl(z,20) + bshl(y,10) + x)
end

local getH
local addTree

function createMap(baseImg,diffImg,path)
	local F2 = io.open(path..'Monde_Arbre','w')
	local F3 = io.open(path..'Monde_Doodads','w')
	if F2 == nil then
		error('Does path ="'..path..'" is valid?')
	end
	addTree = function (x,y,z)
		--local idx = coord2indexTree(x,y)
		local idx = coord2indexObj(x,y,z)
		F2:write(char(band(idx,255),band(bshr(idx,8),255),band(bshr(idx,16),255),band(bshr(idx,24),255) ))
	end
	local function addItem(tp,x,y,z)
		local idx = coord2indexObj(x,y,z)
		F3:write(tp..' '..idx..' ')
	end
	local imgBase = gd.createFromPng(path..baseImg)
	local imgDiff = gd.createFromPng(path..diffImg)
	local function getH(x,y)
		local min = imgBase:red(imgBase:getPixel(x,y))
		local c = imgDiff:getPixel(x,y)
		local red = imgDiff:red(c)
		local h = floor(red * 0.333)
		local slope = 1 + floor( red - h * 3)
		local max = min + h
		if max > 255 then
			error('Too high Z level at x='..x..' y='..y..' (z = '..max..')')
		end
		local tp = 1
		local green = imgDiff:green(c)
		if  green > 128 then
			slope = 1
			tp = 2
			if band(x,3) == 0 and band(y,3) == 0 then
				addTree(x,y,max-1)
			end
		elseif green > 64 then
			tp = 2
		else
			tp = 4
		end
		return max,min,slope,tp
	end

	if imgBase == nil then
		error('File '..path..baseImg.." doesn't exist")
	end
	if imgDiff == nil then
		error('File '..path..diffImg.." doesn't exist")
	end
	local minX,minY = 0, 0
	local maxX,maxY = imgBase:sizeXY()

	local a,b = imgDiff:sizeXY()
	if a~=maxX or b~=maxY then
		error('images should have same size')
	end
	if a > 1023 or b > 1023 then
		error('images should be less than 1024x1024px')
	end
	if startX == nil or startY == nil then
		startX, startY = floor(maxX / 2), floor(maxY / 2)
	end

	addItem('StartingPoint',startX,startY,getH(startX,startY))
	addItem('StartingPoint',startX-1,startY,getH(startX-1,startY))
	addItem('Crystal',startX,startY,getH(startX,startY)-1)
	addTree(startX - 3,startY + 3,getH(startX-3,startY+3)-1)

----[[
	for i = 0,31 do
		os.remove(path..'Monde_'..i)
	end
	local t = 0
	for bY = bshr(minY,8),bshr(maxY+255,8) do
		for bX = bshr(minX,8),bshr(maxX+255,8) do
			local block = 8 * bY + bX
			print(t)
			t = t +1
			local out = char(0,0)
			local needWrite = false
			for j = 0,255 do
				for i = 0,255 do
					local x = 256 * bX + i
					local y = 256 * bY + j
					local min,max,tp,slope = 0,0,0,0

					max,min,slope,tp = getH(x,y)
					if max <= 0 then
						max,min,slope,tp = 0, 0, 0, 0
					else
						needWrite = true
					end
					out = out .. char(min,max,tp,slope)
				end
			end
			if needWrite then
				local F = io.open(path..'Monde_'..block,'wb')
				F:write(out)
				F:close()
			end
		end
	end
--]]
	F2:close()
	F3:close()
end
--loadChunk(path,18)
--saveChunk(path,18,loadPgm(path..'src_'..18..'.pgm') )
--saveChunk2(path,18 )
createMap('base.png','diff.png',path)
